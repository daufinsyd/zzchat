<!-- File login.php -->

<?php
// Define session with english by default
if (!isset($_SESSION)) session_start();
$_SESSION['lan']='en';

// Change language when button is pressed
if (isset($_POST['lang'])) {
    $_SESSION['lan'] = $_POST['lang'];
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>ZZ Chat - Login</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="static/css/signin.css" />
</head>
<body>
    <form class="input-group" method="POST">

            <?php
            // Name button lang
            if($_SESSION['lan'] == 'fr')
                echo "Changer de langue " . "<input class='btn btn-success' type='submit' value='en' name='lang'/>";
            else
                echo "Change language " . "<input class='btn btn-success' type='submit' value='fr' name='lang'/>";
            ?>

    </form>

        <?php
                // include the correct file
                if($_SESSION['lan'] == 'fr')
                    include("./static/langues/fr/login_bienvenu.html");
                else
                    include("./static/langues/en/login_bienvenu.html");
        ?>

</body>
</html>

<?php
    include "php/generalFunctions.php";
    include "php/userFunctions.php";



    // If user wants to login
    if(isset($_POST['login'])) {

		// check that the user entered requested entries
        if (isset($_POST['email']) AND isset($_POST['passwd']) && !(isset($_SESSION['login']) && $_SESSION['login']) && !isUserLogedIn($_POST['email'])) {
            $auth = checkAuthentification($_POST);
            // If true : there is a probblem
            if ($auth) {
                echo '<script src="static/js/signin_register.js"></script>';
                echo '<script type=text/javascript> sign_error(); </script>';
            }
            else {
                // User is loged in
                userLogedIn($_POST['email']);
                redirect('chat.php');
            }
        }
        else {
            if ((isset($_SESSION['login']) && $_SESSION['login']) || isUserLogedIn($_POST['email'])) {  // User is connected
                if (!(isset($_SESSION['login']) && $_SESSION['login'])) {  // if logedIn but not considered loged in by $_SESSION
                    $user = getUserArray($_POST['email']);
                    $_SESSION['name'] = $user['userName'];
                    $_SESSION['email'] = $user['userEmail'];
                    $_SESSION['login'] = 1;

                    // Update / create cookies
                    setCookies($user['userEmail'], $user['userName']);
                }
                redirect('chat.php', 302);  // permanent redirection
            }
            else echo "<strong>Vous n'avez pas entrez toutes les informations demandées. </strong>";
        }
    }
?>
