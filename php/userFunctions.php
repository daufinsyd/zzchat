<?php
/**
 * Created by PhpStorm.
 * User: Sydney Gems
 * Date: 08/10/16
 * Time: 14:07
 *
 * Purpose: all functions related to the users
 */

$GLOBALS['BASE_DIR'] = "/home/etud/sygems/public_html/zzchat/";

// get the good user line in file users.json
function getUserArray($userEmail) {
    $array = "Error";

    $file = $GLOBALS['BASE_DIR'] . "json/users.json";
    if (file_exists($file)) {

        $fileData = file_get_contents($file);  // get data already written
        $json = json_decode($fileData, true);  // decode

        $itemCount = count($json);
        $i = 0;
        while ($i < $itemCount && $json[$i]['userEmail'] != $userEmail ) {  // seek good line
            $i++;
        }
        
        if ($i < $itemCount) {  // if we finded the enter, we return the line
            $array = $json[$i];
        }
    }
    return $array;
}

// return 1 if user is loged in, else 0;
function isUserLogedIn($userEmail) {
    // TODO : copy/past code with getUserArray()
    $response = 0 ;

    $file = $GLOBALS['BASE_DIR'] . "json/connectedUsers.json";
    if (file_exists($file)) {

        $fileData = file_get_contents($file);  // get data already written
        $json = json_decode($fileData, true);  // decode

        $itemCount = count($json);
        $i = 0;
        while ($i < $itemCount && $json[$i]['userEmail'] != $userEmail ) {  // seek good line
            $i++;
        }

        if ($i < $itemCount) {  // if we finded it, we return the good line
            $response = 1;
        }
    }
    return $response;
}

function userLogedIn($userEmail) {
    // Put current user into connectedUsers.json (nickname + email) and login date time

    $dateTime = date('Y/m/d H:i:s');
    $userArray = getUserArray($userEmail);
    $array = array('userName' => $userArray['userName'], 'userEmail' => $userArray['userEmail'], 'loginTime' =>  $dateTime);

    $lock = fopen($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock", 'w'); // Lock connectUsers.json
    fclose($lock);
    // Include lock in writeArrayToFile()
    writeArrayToFile($array, 'connectedUsers');
    if (file_exists($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock")) {
        unlink($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock");  // Unlock the file (remove .lock)
    }

}

function logout($userEmail) {
    // Delete user from connectedUSers.json

    while (file_exists($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock")){};  // If file already in use, wait
    $lock = fopen($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock", 'w'); // Lock connectUsers.json
    fclose($lock);

    $connectedUsers = json_decode(file_get_contents($GLOBALS['BASE_DIR'] . "json/connectedUsers.json"), true);
    $nbItem = count($connectedUsers);
    $i = 0;
    while( $i < $nbItem && $connectedUsers[$i]['userEmail'] != $userEmail ) {
        $i++;
    }
    if ( $connectedUsers[$i]['userEmail'] == $userEmail ) {
        unset($connectedUsers[$i]);
    }
    $connectedUsers = json_encode($connectedUsers);
    $file = fopen($GLOBALS['BASE_DIR'] . "json/connectedUsers.json", 'w');  // rewrite on the file
    fputs($file, $connectedUsers);
    fclose($file);

    unlink($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock");  // Unlock the file (remove .lock)

}
// Function which update / create cookies
function setCookies($userEmail, $userName) {
    $number_of_days = 30;
    $date_of_expiry = time() + 60 * 60 * 24 * $number_of_days;
    setcookie("userEmail", $userEmail, $date_of_expiry, '/', null, false, true);
    setcookie("userName", $userName, $date_of_expiry, '/', null, false, true);
}

// Function which update userName in connectedUsers.json
function updateUser($userEmail, $userName, $userPassword) {
    // Update userName in connectedUsers.json
    while (file_exists($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock")){};  // If file already in use, wait
    $lock = fopen($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock", 'w'); // Lock connectUsers.json
    fclose($lock);

    $connectedUsers = json_decode(file_get_contents($GLOBALS['BASE_DIR'] . "json/connectedUsers.json"), true);
    $nbItem = count($connectedUsers);
    $i = 0;
    while( $i < $nbItem && $connectedUsers[$i]['userEmail'] != $userEmail ) {
    $i++;
    }
    if ( $connectedUsers[$i]['userEmail'] == $userEmail ) {  // update user name and password
        $connectedUsers[$i]['userName'] = $userName;
    }
    $connectedUsers = json_encode($connectedUsers);
    $file = fopen($GLOBALS['BASE_DIR'] . "json/connectedUsers.json", 'w');  // rewrite on the file
    fputs($file, $connectedUsers);
    fclose($file);

    unlink($GLOBALS['BASE_DIR'] . "json/connectedUsers.json.lock");  // Unlock the file (remove .lock)

    // Update userName and userPassword in users.json
    while (file_exists($GLOBALS['BASE_DIR'] . "json/users.json.lock")){};  // If file already in use, wait
    $lock = fopen($GLOBALS['BASE_DIR'] . "json/users.json.lock", 'w'); // Lock connectUsers.json
    fclose($lock);

    $connectedUsers = json_decode(file_get_contents($GLOBALS['BASE_DIR'] . "json/users.json"), true);
    $nbItem = count($connectedUsers);
    $i = 0;
    while( $i < $nbItem && $connectedUsers[$i]['userEmail'] != $userEmail ) {
        $i++;
    }
    if ( $connectedUsers[$i]['userEmail'] == $userEmail ) {  // update user name and password
        $connectedUsers[$i]['userName'] = $userName;
        if (!empty($userPassword)) {
            $connectedUsers[$i]['userPasswd'] = sha1($userPassword);
        }
    }
    $connectedUsers = json_encode($connectedUsers);
    $file = fopen($GLOBALS['BASE_DIR'] . "json/users.json", 'w');  // rewrite on the file
    fputs($file, $connectedUsers);
    fclose($file);

    unlink($GLOBALS['BASE_DIR'] . "json/users.json.lock");  // Unlock the file (remove .lock)
}

// For login.php, 1 = error
function checkAuthentification($data) {
    $code = 1;
    $email = $data['email'];
    $passwd = $data['passwd'];

    $line = file_get_contents($GLOBALS['BASE_DIR'] . "json/users.json");
    $json = json_decode($line, true);

    $nbItem = count($json);
    // for each registered user
    $i = 0;
    while ($i < $nbItem && $json[$i]['userEmail'] != $email) {
        $i++;
    }

    if($i < $nbItem) {
        if (sha1($passwd) == $json[$i]['userPasswd'])  {
            // 0 : no error
            $code = 0;
            $_SESSION['name'] = $json[$i]['userName'];
            $_SESSION['email'] = $json[$i]['userEmail'];
            $_SESSION['login'] = 1;

            // Update / create cookies
            setCookies($_SESSION['email'], $_SESSION['name']);
        }
    }
    return $code;
}

// For register.php

// Function checks if e-mail provided is already used
function emailAlreadyRegistered($email) {
    // 0 = not registered
    $code = 0;
    // Read file which have data user
    $line = file_get_contents($GLOBALS['BASE_DIR'] . "json/users.json");
    $json = json_decode($line, true);

    //$currentEmail = $json[0]['userEmail'];
    //echo "D: " . $currentEmail;

    // For each user registered
    $nbItem = count($json);
    for ($i = 0; $i < $nbItem; $i++){
        $currentEmail = $json[$i]['userEmail'];
        //echo "User: " . $currentEmail . "<br />";

        // If there is a corredpondence between an enter and user registered : we stop, registeration is not valid
        if ($currentEmail == $email) {
            $code = 1;
            break;
        }
    }
    return $code;
}

function checkPassword($passwd, $passwdCheck){
    // 0: ok, 1: too short
    $code = 0;
    // check that the password is enough secured
    $pattern = '/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{6,20}$/';
    // Password and password check are similar ?
    if ($passwd != $passwdCheck) {
        $code = 1;
    }
    elseif (!preg_match($pattern, $_POST['passwd'])) {
        $code = 2;
    }
    return $code;
}

// Function for record user in the file users.json.
function createUser($data){
    // number the password with sha1
    $userPassword = sha1($data['passwd']);
    // create the line in the users file
    $user = array('userName' => $data['name'], 'userEmail' => $data['email'], 'userPasswd' => $userPassword);

    //$encodedUserData = json_encode($user);

    // if the file users.json exist, add the new line of the new user
    if (file_exists($GLOBALS['BASE_DIR'] . "json/users.json")) {
        $fileData = file_get_contents($GLOBALS['BASE_DIR'] . "json/users.json");  // get data already written
        $array = json_decode($fileData, true);  // decode them
        array_push($array, $user);  // add new data
    }
    else {
        $array = array($user);
    }
    $encodedArray = json_encode($array);  // encode in json

    //echo '<br /> E:' . $encodedArray . '<br />';

    $file = fopen($GLOBALS['BASE_DIR'] . "json/users.json", 'w+');  // rewrite on the file
    fputs($file, $encodedArray);
    fclose($file);
}
?>
