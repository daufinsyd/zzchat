<?php
/**
 * Created by PhpStorm.
 * User: sydney_manjaro
 * Date: 08/10/16
 * Time: 15:06
 *
 * Purpose: all functions without relation with the users
 */

// Function redirection to an other page with the code
function redirect($url, $statusCode = 303)
{
    header('Location: ' . $url, true, $statusCode);
    die();
}

function writeArrayToFile($arrayElmt, $fileName, $mode='w+') {

    // If the file *.json exist, we add the new line
    if (file_exists("json/" . $fileName . ".json")) {

        $fileData = file_get_contents("json/" . $fileName . ".json");  // get data already written
        $array = json_decode($fileData, true);  // decode
        array_push($array, $arrayElmt);  // add new data
    }
    else {
        $array = array($arrayElmt);
    }
    $encodedArray = json_encode($array);  // encode in json

    $file = fopen("json/" . $fileName . ".json", $mode);  // rewrite on the file
    fputs($file, $encodedArray);
    fclose($file);
}
?>
