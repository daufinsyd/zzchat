 <!-- File chat.php -->

 <?php
 /**
  * Created by PhpStorm.
  * User: sydney_manjaro
  * Date: 08/10/16
  * Time: 15:06
  *
  * Purpose: all functions without relation with the users
  */

include "php/generalFunctions.php";
include "php/userFunctions.php";

if (!isset($_SESSION['login']) || (isset($_SESSION['login']) && !$_SESSION['login'])) { redirect("login.php"); }
else {
    if(isset($_POST['updateAccount'])) {
        $userArray = getUserArray($_SESSION['email']);
        if(!empty($_POST['oldPasswd']) && isset($_POST['newPasswd']) && isset($_POST['newPasswd2'])) {  // if newpass empty : user doens't want to change his password
			// Password provided is correct
            if(sha1($_POST['oldPasswd']) == $userArray['userPasswd']) {
                updateUser($_SESSION['email'], $_POST['newUserName'], $_POST['newPasswd']);
            }
            else echo "<strong>Bad old password</strong>";
        }
    }
}

if (isset($_GET['logout'])) {
    logout($_SESSION['email']);
    $_SESSION['login'] = false;
    redirect('index.php', 302);
}
else if (isset($_POST['send'])){
    if(!empty($_POST['textToSend'])) {
        //echo $_POST['textToSend'];
        $dateTime = date('Y/m/d H:i:s');
        $msg = array("userName" => $_SESSION['name'], "dateTime" => $dateTime, "message" => htmlspecialchars($_POST['textToSend']));
        writeArrayToFile($msg, 'msgList');
    }
}
else if (isset($_POST['lang'])) {
    $_SESSION['lan'] = $_POST['lang'];
}
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="static/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="static/css/chat.css" />
 <!-- Simple body by default with boostrap -->

<div class="container">
    <div class="row">
        <form class="input-group" method="POST">
			
            <?php	//Possibility to change the language
            if($_SESSION['lan'] == 'fr')
                echo "Changer de langue " . "<input class='btn btn-success' type='submit' value='en' name='lang';/>";
            else
                echo "Change language " . "<input class='btn btn-success' type='submit' value='fr' name='lang';/>";


            if($_SESSION['lan'] == 'fr')
                echo "<br/>Bienvenue ";
            else
                echo "<br/>Welcome ";
            echo "<span id='userName'>" . $_SESSION['name'] . "</span>";
            ?>
            
        </form>
        <div class="col-lg-3" id="account">
            <img class="avatar img-circle" src="static/img/logoWhite.png" alt="avatar"><br />
            <form class="form-signin" method="post">
                <input type="text" name="newUserName" value="<?php echo $_SESSION['name']; ?>" class="form-control">
                <br /><br />

                <?php
                        if($_SESSION['lan'] == 'fr')
                            include("./static/langues/fr/chat_nouv_mdp.html");
                        else
                            include("./static/langues/en/chat_nouv_mdp.html");
                ?>

        <div class="panel panel-primary col-lg-6">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-log-out"></span>
                <i class="glyphicon glyphicon-home"></i>
                <span class="glyphicon glyphicon-comment"></span>
                <div class="panel-body" id="divMsg">
                    <ul class="chat" id="chatMessages">
                        <li class="left clearfix"><span class="chat-img pull-left"><img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" /></span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="left clearfix"><span class="chat-img pull-left"><img src="http://placehold.it/50/FA6F57/fff&text=" alt="User Avatar" class="img-circle" /></span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                    <strong class="pull-left primary-font">Bhaumik Patel</strong>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="left clearfix"><span class="chat-img pull-  left"><img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" /></span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                                        <span class="glyphicon glyphicon-time"></span>14 mins ago</small>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                        <li class="right clearfix"><span class="chat-img pull-right"><img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" /></span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>15 mins ago</small>
                                    <strong class="pull-right primary-font">Bhaumik Patel</strong>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
                    </ul>

                    <?php
                    // Get list of connected members
                    $msgList = json_decode(file_get_contents("json/msgList.json"), true);
                    $nbItem = count($msgList);
                    echo "<ul class=\"chat\">";
                    for ($i = 0; $i < $nbItem; $i++) {
                        $userName = $msgList[$i]['userName'];  // if writted in echo ... render: Array['userName']
                        $dateTime = $msgList[$i]['dateTime'];
                        $msg = $msgList[$i]['message'];
                        echo "<li class=\"right clearfix\"><span class=\"chat-img pull-right\"><img src=\"http://placehold.it/50/FA6F57/fff&text=ME\" alt=\"User Avatar\" class=\"img-circle\" /></span>
                            <div class=\"chat-body clearfix\">
                                <div class=\"header\">
                                    <small class=\" text-muted\"><span class=\"glyphicon glyphicon-time\"></span>$dateTime</small>
                                    <strong class=\"pull-right primary-font\">$userName</strong>
                                </div>
                                <p>
                                    $msg
                                </p>
                            </div>
                        </li>";
                    }
                    echo "</ul>";
                    ?>

                </div>
                <div class="panel-footer">
                    <!--<div class="input-group">-->

                    <!--</div>-->
                    <form class="input-group" method="POST">
                        <input id="btnBold" type="button" onclick="formatText ('b');" class="btn btn-xs btn-info"/>
                        <input id="btnItalic" type="button" onclick="formatText('i')" class="btn btn-xs btn-info"/>
                        <input id="btnUnder" type="button" onclick="formatText('u')" class="btn btn-xs btn-info"/>
                        <input id="btnQuote" type="button" onclick="formatText('quote')" class="btn btn-xs btn-info"/>
                        <input id="btn-input" name="textToSend" type="text" class="form-control input-sm" placeholder="Type your message here..." />
                        <span class="input-group-btn"><button class="btn btn-warning btn-sm" id="btn-chat" name="send">Send</button></span>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-3" id="connectedMembers">

<?php
		if($_SESSION['lan'] == 'fr')
			include("./static/langues/fr/chat_membres_co.html");
		else
			include("./static/langues/en/chat_membres_co.html");
?>

<!--chat.php.membres_co-->
            <img class="avatar img-circle" src="static/img/logoWhite.png" alt="avatar"><br /><br />
            <div id="connectedmembersList">
                <?php
                    // Get list of connected members
                    $connectedUsers = json_decode(file_get_contents("json/connectedUsers.json"), true);
                    $nbItem = count($connectedUsers);
                    echo "<ul>";  // create <uL>
                    for ($i = 0; $i < $nbItem; $i++) {
                        $userName = $connectedUsers[$i]['userName'];  // if writted in echo ... render: Array['userName']
                        echo "<li>$userName</li>";  // add item
                    }
                    echo "</ul>";  // end </ul>
                ?>
                
            </div>
        </div>
    </div>
</div>

 <!-- JQuery -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script>

 <script>
     function formatText(tag) {
         // add asked tag in the text field
         var Field = document.getElementById('btn-input'); // get text input
         var val = Field.value;  // get the entire text
         var selected_txt = val.substring(Field.selectionStart, Field.selectionEnd);  // get highlighted text
         var before_txt = val.substring(0, Field.selectionStart);
         var after_txt = val.substring(Field.selectionEnd, val.length);
         Field.value = before_txt + '[' + tag + ']' + selected_txt + '[/' + tag + ']' + after_txt;
     }

     function scrollBar() {
         // scroll the scrollbar down
         var objDiv = document.getElementById("divMsg");
         objDiv.scrollTop = objDiv.scrollHeight;
     }

 </script>
 <script type="text/javascript">
     $(document).ready(function() {  // when the webpage is fully loaded
     function updateData(){
         // Update connected members list
         $("#connectedmembersList").html('');
         // create ajax request
         $.ajax({
             url: 'php/listConnectedMembers.php',  // return connectedMembers as an json array
             dataType: "json",
             type: 'GET',
             success: function(data) {
                 if (data) {
                     $('#connectedmembersList').html("<ul/>");  // create <ul> element
                     $.each(data, function(i, user){
                         // Append user to <div id=connectedmembersList><ul></ul></div>
                         $('#connectedmembersList').find('ul').append('<li>' + user.userName + '</li>');
                     });
                 }
             }
             });
         }

     function updateMsg(){
             // Update messages
             $("#divMsg").html('');
             $.ajax({
                 url: 'php/listMessages.php',  // return connectedMembers as an json array
                 dataType: "json",
                 type: 'GET',
                 success: function(data) {
                     if (data) {
                         $('#divMsg').html("<ul style='list-style: none;' />");  // create <ul> element
                         $.each(data, function(i, msg){
                             // Append user to <div id=connectedmembersList><ul></ul></div>

                             // BBCODE
                             msg.message = msg.message.replace("[b]", "<b>");
                             msg.message = msg.message.replace("[/b]", "</b>");
                             msg.message = msg.message.replace("[i]", "<i>");
                             msg.message = msg.message.replace("[/i]", "</i>");
                             msg.message = msg.message.replace("[u]", "<u>");
                             msg.message = msg.message.replace("[/u]", "</u>");
                             msg.message = msg.message.replace("[quote]", "<span class=\"quote\">");
                             msg.message = msg.message.replace("[/quote]", "</spa,>");
                             msg.message = msg.message.replace(":-)", "&#12484;");
                             msg.message = msg.message.replace(":-D", "&#128515;");
                             msg.message = msg.message.replace("^^", "&#128516;");
                             msg.message = msg.message.replace(":-/", "&#128533;");
                             msg.message = msg.message.replace(":-|", "&#128528;");
                             msg.message = msg.message.replace(":-o", "&#128558;");
                             msg.message = msg.message.replace("D-:", "&#128550;");
                             msg.message = msg.message.replace("(-:", "<img style='width: 20px; height: 20px;' src=\"static/img/smile_mozilla\">");
                             msg.message = msg.message.replace("(:", "<img style='width: 20px; height: 20px;' src=\"static/img/smile_mozilla\">");
                             msg.message = msg.message.replace(":-(", "&#9785;");
                             msg.message = msg.message.replace(":'(", "&#128549;");


                             var usrName = document.getElementById("userName").innerHTML;

                             /*$('#divMsg').find('ul').append('' +
                                 '<li class="left clearfix"><div class="chat-body clearfix"><div class="header"><strong class="primary-font">VOUS ' + msg.userName + '</strong><small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span>' + msg.dateTime + '</small></div><p>' + msg.message + '</p></div></li>'
                             );*/

                             if (usrName == msg.userName) {
                                 $('#divMsg').find('ul').append('' +
                                     '<li class="right clearfix"><div class="chat-body clearfix" style="width:100%; text-align: right; background-color: #1CB94A;"><div class="header" ><strong class="primary-font">' + msg.userName + '</strong><small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span> ' + msg.dateTime + '</small></div><p class="msgRight">' + msg.message + '</p></div></li>'
                                 );
                             }
                             else {
                                 $('#divMsg').find('ul').append('' +
                                     '<li class="left clearfix"><div class="chat-body clearfix" style="background-color: #9acfea"><div class="header"><strong class="primary-font">' + msg.userName + '</strong><small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span> ' + msg.dateTime + '</small></div><p class="msgLeft">' + msg.message + '</p></div></li>'
                                 );
                             }
                         });
                     }
                 }
             });
            setTimeout(scrollBar, 200);  // scrollbar is scrolled 0.1s later (thus there isn't any problem)
         }

     updateData();  // first execution
     updateMsg();
     setTimeout(scrollBar, 500);  // scroll down on first load or on message sent

     setInterval(updateData, 5000);  // page reloaded 5s later
     setInterval(updateMsg, 5000);
     //setInterval(scrollBar, 8200);  // scroll down a little after div is completed
     });
</script>
