/**
 * Created by sydney_manjaro on 18/09/16.
 */
$(function() {

    $('#login-form-link').click(function(e) {
        $("#login-form").delay(100).fadeIn(100);
        $("#register-form").fadeOut(100);
        $('#register-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });
    $('#register-form-link').click(function(e) {
        $("#register-form").delay(100).fadeIn(100);
        $("#login-form").fadeOut(100);
        $('#login-form-link').removeClass('active');
        $(this).addClass('active');
        e.preventDefault();
    });

});

// To create button
function createButton(context, value, func){
    var button = document.createElement("input");
    button.type = "button";
    button.value = value;
    button.onclick = func;

    button.setAttribute("style", "color:red; font-style:bold");
    context.appendChild(button);
}


// Display a error message if the password does not satisfy requirements
function reg_passwdMissSpelling() {
    console.log("Ajout de passwd carac");
    var newItem = document.createElement("span");  // create an element <span></span>
    newItem.setAttribute("style", "color:red; font-style:italic");
    var textNode = document.createTextNode("Votre mot de passe doit comporter des chiffres, des lettres (minuscules et majuscules) des caractères spéciaux et au moins 6 caractères");  // Ajout un node #
    newItem.appendChild(textNode);  // add the node text to <span>

    var form = document.getElementById("registerForm");

    form.insertBefore(newItem, form.childNodes[1]);  // add the element <span></span> to <form></form>
}


// Display an error message if the email already exist
function reg_emailAlreadyExists() {
    console.log("Ajout de emailAlreadyExists");
    var newItem = document.createElement("span");
    newItem.setAttribute("style", "color:red; font-style:italic");
    var textNode = document.createTextNode("L'adresse email entrée est déjà utilisée.");
    newItem.appendChild(textNode);

    var form = document.getElementById("registerForm");

    form.insertBefore(newItem, form.childNodes[1]);
}

// Display an error message if the verification failed
function sign_error() {
    console.log("Erreur lors de l'authentification");
    var newItem = document.createElement("span");
    newItem.setAttribute("style", "color:red; font-style:bold");
    var textNode = document.createTextNode("Votre mot de passe et votre identifiant ne correspondent pas.");
    newItem.appendChild(textNode);

    var form = document.getElementById("loginForm");
    form.insertBefore(newItem, form.childNodes[2]);
}

function alreadyLoged() {
    console.log("Already connected > you may wan to be redirected to the chat or disconnect");
    // Add message
    var newItem = document.createElement("span");
    newItem.setAttribute("style", "color:red; font-style:bold");
    var textNode = document.createTextNode("Vous êtes déjà connecté.");
    newItem.appendChild(textNode);

    var form = document.getElementById("loginForm");
    form.insertBefore(newItem, form.childNodes[2]);

    // Add disconnect button
    createButton(form, "Logout", function(){
        highlight(form.childNodes[2]);
        // Example of different context, copied function etc
        // createButton(this.parentNode, this.onclick);
    });
    // Add redirect to chat button
    createButton(form, "Au chat", function() {window.location = "chat.php";});
}
