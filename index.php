<!DOCTYPE html>
<html lang="en">

<head>

    <title>ZZ Chat</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">

    <!-- Custom Fonts -->
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="static/css/agency.min.css" rel="stylesheet">

</head>

<?php
/**
 * Created by PhpStorm.
 * User: Sydney Gems
 * Date: 26/09/16
 * Time: 18:30
 */

if (!isset($_SESSION)) session_start();

include "php/userFunctions.php";

// Initialisation des cookies
if(isset($_SESSION['login']) && $_SESSION['login']) {
    $number_of_days = 30;
    $date_of_expiry = time() + 60 * 60 * 24 * $number_of_days;


    setcookie("userEmail", $_SESSION['email'], $date_of_expiry, '/', null, false, true);
    setcookie("userName", $_SESSION['name'], $date_of_expiry, '/', null, false, true);
    // le dernier 'true' active httpOnly ce qui permet d'éviter des attaquues par XSS (si oublie de htmlspecialchars)
}

?>

<body id="page-top" class="index" style="background-image: url('static/img/background.jpg');">
	

    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <div class="navbar-header page-scroll">
                <a class="navbar-brand page-scroll" href="login.php">ZZ chat</a>
            </div>
        </div>
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">
                    <?php
                    if(isset($_COOKIE['userName']))
                        echo "Content de vous revoir " . $_COOKIE['userName'];
                    else
                        echo "Bonjour inconnu"
                    ?>
                </div>
                <div class="intro-heading">
                    <?php
                    if(isset($_COOKIE['userName']))
						// Redirection to page login
                        echo "Envie de vous connecter ? <a href=\"login.php\" class=\"page-scroll btn btn-xl\">Cliquez ici</a>";
                    else
						// Redirection to register
                        echo "Pourquoi ne pas vous enregistrer? <a href=\"register.php\" class=\"page-scroll btn btn-xl\">Cliquez ici</a>";
                    ?>
                </div>

            </div>
        </div>
    </header>
</body>

</html>
