<!-- File register.php -->

<?php
if (!isset($_SESSION)) session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <title>Register</title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.4/css/bootstrap.min.css" integrity="sha384-2hfp1SzUoho7/TsGGGDaFdsuuDL0LX2hnUp6VkX3CUQ2K4K+xjboZdsXyp4oUHZj" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="static/css/signin.css" />
    
</head>
<body>

<?php
	// include file with the good language
	if($_SESSION['lan'] == 'fr')
		include("./static/langues/fr/register_formulaire.html");
	else
		include("./static/langues/en/register_formulaire.html");
?>

<?php

    include "php/userFunctions.php";
    include "php/generalFunctions.php";

    // If the user wants to register
    if(isset($_POST['register'])){
		


        // heart of programm
        $pass = checkPassword($_POST['passwd'], $_POST['passwd2']);
        if($pass == 0) {
            $test=emailAlreadyRegistered($_POST['email']);
            if ($test == 1) $pass = 3;
        }
        switch ($pass){
            case 0: echo 'Parfait :D <br />';
                    createUser($_POST);
                    redirect('login.php');
                break;
            case 1: echo 'Vos deux mots de passes ne correspondent pas!<br />';
                break;
            case 2: echo '<script src="static/js/signin_register.js">
                         </script><script type="text/javascript">
                            reg_passwdMissSpelling();
                         </script>';
                break;
            case 3: echo '<script src="static/js/signin_register.js">
                         </script><script type="text/javascript">
                            reg_emailAlreadyExists();
                         </script>';
        }
    }
?>
    <script src="static/js/signin_register.js"></script>
</body>
</html>
