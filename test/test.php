<?php
/**
 * Created by PhpStorm.
 * User: sygems
 * Date: 07/12/16
 * Time: 12:57
 */


/*
 *
 *     function odd_or_even( $num ) {
            return $num%2; // Returns 0 for odd and 1 for even
        }
 *
 *
 */


require_once('../php/userFunctions.php');
require('../php/generalFunctions.php');
//require('../php/userFunctions.php');
use PHPUnit\Framework\TestCase;
class test extends \PHPUnit_Framework_TestCase {
    public function test_checkAuth() {

        // pass !=
        $_POST['passwd'] = "a";
        $this->assertTrue( checkPassword("a", "b") == 1 );
        /* Regex not respected */
        $_POST['passwd'] = "a";
        $this->assertTrue( checkPassword("a", "a") == 2 );
        $_POST['passwd'] = "azerttrtr";
        $this->assertTrue( checkPassword("azerttrtr", "azerttrtr") == 2 );
        $_POST['passwd'] = "azerttrtrAA456";
        $this->assertTrue( checkPassword("azerttrtrAA456", "azerttrtrAA456") == 2 );
        $_POST['passwd'] = "aA1!";
        $this->assertTrue( checkPassword("aA1!", "aA1!") == 2 );
        $_POST['passwd'] = "AZ123456789!";
        $this->assertTrue( checkPassword("AZ123456789!", "AZ123456789!") == 2 );
        $_POST['passwd'] = "!:,;!:,;!:,;!";
        $this->assertTrue( checkPassword("!:,;!:,;!:,;!", "!:,;!:,;!:,;!") == 2 );
        /* Regex respected */
        $_POST['passwd'] = "Azerty123!";
        $this->assertTrue( checkPassword("Azerty123!", "Azerty123!") == 0 );
    }
    
    public function test_checkEmail() {
        /*unlink("json/users.json");
        $file = fopen('json/users.json', 'w+');  // rewrite on the file
        fputs($file, '[{"userName":"a","userEmail":"a@a.net","userPasswd":"$1$NBhS4HfQ$uK071X.XYsQnXVSz\/ezXk0"}]');
        fclose($file);
        $data = array('passwd' => "a", "name" => 'test', "email" => 'test@test.net_test');
        createUser($data);
        */

        /**/
        $this->assertTrue( emailAlreadyRegistered("!") == 0 );
        /* Ok email */
        $data['passwd'] = "at";
        $data['name'] = "testi";
        $data['email'] = "a@a.net";
        createUser($data);
        $this->assertTrue( emailAlreadyRegistered("a@a.net") == 1 );
    }
    

    public function test_checkGetUserArray() {
        /* There is an absolute path so we have to meet users in user.json in public_html */

        /* Doesn't exist */
        $array = getUserArray("!");
        $this->assertTrue( $array == 'Error' );

        /* Exists */
        $array = getUserArray("c@c.net");
        $this->assertTrue( $array != 'Error' );
        $this->assertTrue( $array['userName'] == 'Name :D' );
        $this->assertTrue( $array['userEmail'] == 'c@c.net' );
        $this->assertTrue( $array['userPasswd'] == sha1('Azer123!') );
    }

    public function test_checkIsUserLogedIn() {
        /* There is an absolute path so we have to meet users in connectedUsers.json in public_html */

        // User loged out
        $this->assertTrue( isUserLogedIn("!") == 0);

        // User conneted
        $this->assertTrue( isUserLogedIn("david@david.david") == 1);
    }
}
?>
